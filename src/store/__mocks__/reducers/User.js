export default {
  session: {
    id: 24,
    username: "demo"
  },
  user_details: {
    id: 24,
    username: "demo",
    firstname: "iam_",
    surname: "demo",
    email: "demo",
    bio: "",
    account_type: "public",
    email_verified: "yes",
    joined: "1480114098767",
    twitter: "",
    facebook: "",
    github: "",
    instagram: "",
    phone: "",
    website: "",
    isOnline: false,
    lastOnline: ""
  },
  tags: [
    {
      tag: "traveler",
      user: 24
    },
    {
      tag: "nature-lover",
      user: 24
    }
  ],
  mutualUsers: [
    {
      user: 11,
      username: "demo",
      firstname: "demo",
      surname: "demo",
      follow_id: 261,
      mutualUsersCount: 0
    }
  ]
};
