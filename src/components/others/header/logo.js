import React from 'react'
import AppLink from '../link/link'

const HeaderLogo = () => (
  <div className="logo">
    <AppLink url="/">
      <img src="/images/....jpg" alt="..." />
    </AppLink>
  </div>
)

export default HeaderLogo
